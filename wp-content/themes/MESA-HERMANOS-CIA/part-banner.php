<?php if ( is_front_page() || is_page( array( 'quienes-somos', 'productos-quimicos', 'productos-naturales', 'manufacturas-de-corcho', 'colorantes-y-pigmentos', 'gomas-y-ceras', 'especializados-para-la-industria', 'contacto' ) ) ) : ?>
<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'quienes-somos' ) ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif; ?>
				<?php if ( is_page( array( 'productos-quimicos' ) ) ) : dynamic_sidebar( 'banner_productos_quimicos' ); endif; ?>
				<?php if ( is_page( array( 'productos-naturales' ) ) ) : dynamic_sidebar( 'banner_productos_naturales' ); endif; ?>
				<?php if ( is_page( array( 'manufacturas-de-corcho' ) ) ) : dynamic_sidebar( 'banner_manufacturas_de_corcho' ); endif; ?>
				<?php if ( is_page( array( 'colorantes-y-pigmentos' ) ) ) : dynamic_sidebar( 'banner_colorantes_y_pigmentos' ); endif; ?>
				<?php if ( is_page( array( 'gomas-y-ceras' ) ) ) : dynamic_sidebar( 'banner_gomas_y_ceras' ); endif; ?>
				<?php if ( is_page( array( 'especializados-para-la-industria' ) ) ) : dynamic_sidebar( 'banner_especializados_para_la_industria' ); endif; ?>
				<?php if ( is_page( array( 'contacto' ) ) ) : dynamic_sidebar( 'banner_contacto' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->
<?php endif; ?>
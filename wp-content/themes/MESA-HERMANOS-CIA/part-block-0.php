<!-- Begin Block 0 -->
	<section class="block_0" data-wow-delay="0.5s">
		<div class="row expanded">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_0' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 0 -->
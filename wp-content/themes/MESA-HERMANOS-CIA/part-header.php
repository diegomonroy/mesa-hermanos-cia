<?php
$style = '';
if ( is_front_page() || is_page( array( 'quienes-somos' ) ) ) : else :
	$style = 'special';
endif;
?>
<!-- Begin Top -->
	<section class="top_1" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_1' ); ?>
			</div>
		</div>
	</section>
	<section class="top <?php echo $style; ?>" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-7 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
			<div class="small-7 medium-2 columns">
				<?php dynamic_sidebar( 'search' ); ?>
			</div>
			<!--<div class="small-6 medium-2 columns">
				<?php /*dynamic_sidebar( 'language' );*/ ?>
			</div>-->
			<!--<div class="small-3 medium-1 columns">
				<?php /*dynamic_sidebar( 'pse' )*/ ?>
			</div>-->
			<?php /*dynamic_sidebar( 'search_top' );*/ ?>
		</div>
	</section>
<!-- End Top -->
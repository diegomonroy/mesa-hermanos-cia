<?php
global $wp_query;
$theme = 'popup';
$post_type = get_query_var( 'post_type' );
if ( $wp_query->is_search && $post_type == 'product' ) {
	$theme = '';
}
if ( is_product_category( array(
	'carrito-productos-quimicos',
	'carrito-productos-naturales',
	'carrito-manufacturas-de-corcho',
	'carrito-colorantes-y-pigmentos',
	'carrito-gomas-y-ceras',
	'carrito-especializados-para-la-industria'
) ) ) {
	$theme = '';
}
if ( $post_type == 'product' ) {
	if ( has_term( array(
		'carrito-productos-quimicos',
		'carrito-productos-naturales',
		'carrito-manufacturas-de-corcho',
		'carrito-colorantes-y-pigmentos',
		'carrito-gomas-y-ceras',
		'carrito-especializados-para-la-industria'
	), 'product_cat' ) ) {
		$theme = '';
	}
}
?>
<?php get_header( $theme ); ?>
	<?php get_template_part( 'part', 'woocommerce' ); ?>
<?php get_footer( $theme ); ?>
// JavaScript Document

/* ************************************************************************************************************************

MESA HERMANOS & CIA

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2018

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {
	/* Menu */
	jQuery( 'ul.sub-menu' ).addClass( 'menu' );
	/* Newsletter */
	jQuery( '.tnp-email' ).attr( 'placeholder', 'insertar correo aquí' );
	jQuery( 'input.tnp-submit' ).prop( 'value', 'ENVIAR' );
	/* Search */
	jQuery( '.moduletable_to6' ).hide();
	jQuery( '.moduletable_to6' ).append( '<a href="#" id="close-search" class="button close">Cerrar</a>' );
	jQuery( 'a#open-search' ).click(function () {
		jQuery( '.moduletable_to6' ).slideDown( 1000 );
		return false;
	});
	jQuery( 'a#close-search' ).click(function () {
		jQuery( '.moduletable_to6' ).slideUp( 1000 );
		return false;
	});
});
<?php
$column = '';
if ( is_product_category( array(
	'carrito-productos-quimicos',
	'carrito-productos-naturales',
	'carrito-manufacturas-de-corcho',
	'carrito-colorantes-y-pigmentos',
	'carrito-gomas-y-ceras',
	'carrito-especializados-para-la-industria'
) ) ) {
	$column = 'medium-9';
}
if ( $post_type == 'product' ) {
	if ( has_term( array(
		'carrito-productos-quimicos',
		'carrito-productos-naturales',
		'carrito-manufacturas-de-corcho',
		'carrito-colorantes-y-pigmentos',
		'carrito-gomas-y-ceras',
		'carrito-especializados-para-la-industria'
	), 'product_cat' ) ) {
		$column = 'medium-9';
	}
}
?>
<?php get_template_part( 'part', 'block-0' ); ?>
<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row">
			<?php
			if ( is_product_category( array(
				'carrito-productos-quimicos',
				'carrito-productos-naturales',
				'carrito-manufacturas-de-corcho',
				'carrito-colorantes-y-pigmentos',
				'carrito-gomas-y-ceras',
				'carrito-especializados-para-la-industria'
			) ) ) {
			?>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'left' ); ?>
			</div>
			<?php } ?>
			<?php
			if ( $post_type == 'product' ) {
				if ( has_term( array(
					'carrito-productos-quimicos',
					'carrito-productos-naturales',
					'carrito-manufacturas-de-corcho',
					'carrito-colorantes-y-pigmentos',
					'carrito-gomas-y-ceras',
					'carrito-especializados-para-la-industria'
				), 'product_cat' ) ) {
					?>
					<div class="small-12 medium-3 columns">
						<?php dynamic_sidebar( 'left' ); ?>
					</div>
					<?php
				}
			}
			?>
			<div class="small-12 <?php echo $column; ?> columns">
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->
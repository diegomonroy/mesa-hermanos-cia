<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/animate.css/animate.min.css', false );
	//wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/foundation/js/vendor/jquery.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/foundation/js/vendor/what-input.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	//wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/wow/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' ),
			'product-menu' => __( 'Product Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add theme support
 */
add_theme_support( 'post-thumbnails' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Top 1',
			'id' => 'top_1',
			'before_widget' => '<div class="moduletable_to11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search',
			'id' => 'search',
			'before_widget' => '<div class="moduletable_to3">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Top',
			'id' => 'search_top',
			'before_widget' => '<div class="moduletable_to6 text-right">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Language',
			'id' => 'language',
			'before_widget' => '<div class="moduletable_to4 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'PSE',
			'id' => 'pse',
			'before_widget' => '<div class="moduletable_to5 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Quiénes Somos',
			'id' => 'banner_quienes_somos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Productos Químicos',
			'id' => 'banner_productos_quimicos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Productos Naturales',
			'id' => 'banner_productos_naturales',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Manufacturas de Corcho',
			'id' => 'banner_manufacturas_de_corcho',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Colorantes y Pigmentos',
			'id' => 'banner_colorantes_y_pigmentos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Gomas y Ceras',
			'id' => 'banner_gomas_y_ceras',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Especializados para la Industria',
			'id' => 'banner_especializados_para_la_industria',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Contacto',
			'id' => 'banner_contacto',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left',
			'id' => 'left',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 0',
			'id' => 'block_0',
			'before_widget' => '<div class="moduletable_b01 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 4',
			'id' => 'block_4',
			'before_widget' => '<div class="moduletable_b41">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="text-center">',
			'after_title' => '</h3>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'WhatsApp',
			'id' => 'whatsapp',
			'before_widget' => '<div class="moduletable_wa1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Functions to declare WooCommerce image sizes
 */
function woocommerce_catalog_image( $size ) {
	return array(
		'width' => 220,
		'height' => 150,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_thumbnail', 'woocommerce_catalog_image' );

function woocommerce_single_image( $size ) {
	return array(
		'width' => 620,
		'height' => 420,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_single', 'woocommerce_single_image' );

function woocommerce_gallery_image( $size ) {
	return array(
		'width' => 200,
		'height' => 200,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'woocommerce_gallery_image' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 8;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/*
 * Function to remove image in WooCommerce products detail page
 */
// remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );

/*
 * Function to remove image in WooCommerce products
 */
function remove_images() {
	global $product;
	$array = array(
		'productos-quimicos',
		'productos-naturales',
		'manufacturas-de-corcho',
		'colorantes-y-pigmentos',
		'gomas-y-ceras',
		'especializados-para-la-industria'
	);
	if ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
		if ( is_product_category( $array ) || has_term( $array, 'product_cat' ) ) {
			remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
			remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
			wp_enqueue_style( 'special-css', get_template_directory_uri() . '/build/special.css', false );
		}
	}
}
add_action( 'template_redirect', 'remove_images' );

/*
 * Custom hooks in WooCommerce
 */
function custom_description() {
	the_content();
}
add_action( 'woocommerce_single_product_summary', 'custom_description', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function custom_clear() {
	echo '
		<div class="clear"></div>
		<div class="text-center">
			<button class="hollow button" onclick="window.history.go(-1);">Volver</button>
		</div>
	';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_clear', 10 );

/*
 * Custom shortcode to Log In
 */
function shortcode_log_in() {
	$web = get_site_url() . '/';
	if ( is_user_logged_in() ) {
		global $current_user;
		get_currentuserinfo();
		$username = $current_user->user_login;
		$my_account = $web . 'mi-cuenta/';
		$my_orders = $web . 'productos/mi-cuenta/';
		$logout = $web . 'cerrar-sesion/';
		$html = 'Bienvenido/a! ' . $username . ' | <a href="' . $my_account . '">Mi Cuenta</a> | <a href="' . $my_orders . '">Mis Pedidos</a> | <a href="' . $logout . '">Cerrar Sesión</a>';
	} else {
		$login = $web . 'inicia-sesion/';
		$register = $web . 'crear-una-cuenta/';
		$html = 'Bienvenido/a! <a href="' . $login . '"><strong>Inicia sesión</strong></a> o <a href="' . $register . '"><strong>Crear una cuenta</strong></a>';
	}
	return $html;
}
add_shortcode( 'log_in', 'shortcode_log_in' );